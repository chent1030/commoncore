package com.base.core.service;

import com.base.core.base.BaseService;
import com.base.core.common.vo.SearchVO;
import com.base.core.entity.Log;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 * @author chentao
 */
public interface LogService extends BaseService<Log,String> {

    /**
     * 分页搜索获取日志
     * @param type
     * @param key
     * @param searchVO
     * @param pageable
     * @return
     */
    Page<Log> findByConfirm(Integer type, String key, SearchVO searchVO, Pageable pageable);

    /**
     * 删除所有
     */
    void deleteAll();
}
