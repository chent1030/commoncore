package com.base.core.service;

import com.base.core.base.BaseService;
import com.base.core.entity.Role;
import com.base.core.entity.UserRole;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

/**
 * @author chentao
 */
@CacheConfig(cacheNames = "userRole")
public interface UserRoleService extends BaseService<UserRole, String> {

    /**
     * 通过用户id获取
     * @param userId
     * @return
     */
    @Cacheable(key = "#userId")
    @Query(value = "select r from UserRole u left join Role r on u.roleId = r.id where u.userId = ?1")
    List<Role> findByUserId(String userId);

    /**
     * 通过用户id获取用户角色关联的部门数据
     * @param userId
     * @return
     */
    @Query(value = "select d.departmentId from UserRole u left join RoleDepartment d on u.roleId = d.roleId where u.userId = ?1")
    List<String> findDepIdsByUserId(String userId);
}
