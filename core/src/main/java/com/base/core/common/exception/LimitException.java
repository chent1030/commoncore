package com.base.core.common.exception;

import lombok.Data;

/**
 * 限流异常
 * @author chentao
 */
@Data
public class LimitException extends RuntimeException {

    private String errMsg;

    public LimitException(String errMsg) {
        super(errMsg);
        this.errMsg = errMsg;
    }
}
