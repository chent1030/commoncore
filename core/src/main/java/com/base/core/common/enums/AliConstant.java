package com.base.core.common.enums;

/**
 * @author chentao
 */

public enum AliConstant {

    /**
     * 阿里短信 通用
     */
    ALI_SMS_COMMON("ALI_SMS_COMMON"),

    /**
     * 阿里短信 登录验证码
     */
    ALI_SMS_LOGIN("ALI_SMS_LOGIN"),

    /**
     * 阿里短信 注册验证码
     */
    ALI_SMS_REGIST("ALI_SMS_REGIST"),

    /**
     * 阿里短信 修改手机
     */
    ALI_SMS_CHANGE_MOBILE("ALI_SMS_CHANGE_MOBILE"),

    /**
     * 阿里短信 修改密码
     */
    ALI_SMS_CHANG_PASS("ALI_SMS_CHANG_PASS"),

    /**
     * 阿里短信 重置密码
     */
    ALI_SMS_RESET_PASS("ALI_SMS_RESET_PASS"),

    /**
     * 阿里短信 工作流消息
     */
    ALI_SMS_ACTIVITI("ALI_SMS_ACTIVITI");

    private String value;

    AliConstant(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}
