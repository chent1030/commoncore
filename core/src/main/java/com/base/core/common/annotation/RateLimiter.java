package com.base.core.common.annotation;

import java.lang.annotation.*;

/**
 * @author chentao
 * 限流
 */
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface RateLimiter {

    int limit() default 5;

    long timeout() default 1000;
}
