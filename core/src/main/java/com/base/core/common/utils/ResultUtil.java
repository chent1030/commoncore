package com.base.core.common.utils;


import com.base.core.common.vo.Result;

/**
 * @author chentao
 */
public class ResultUtil<T> {

    private Result<T> result;

    public ResultUtil(){
        result = new Result<>();
        result.setErrMsg("success");
        result.setErrCode(200);
    }

    public Result<T> setData(T t){
        this.result.setResult(t);
        this.result.setErrCode(200);
        return this.result;
    }

    public Result<T> setSuccessMsg(String msg){
        this.result.setErrMsg(msg);
        this.result.setErrCode(200);
        this.result.setResult(null);
        return this.result;
    }

    public Result<T> setData(T t, String msg){
        this.result.setResult(t);
        this.result.setErrCode(200);
        this.result.setErrMsg(msg);
        return this.result;
    }

    public Result<T> setErrorMsg(String msg){
        this.result.setErrMsg(msg);
        this.result.setErrCode(500);
        return this.result;
    }

    public Result<T> setErrorMsg(Integer code, String msg){
        this.result.setErrMsg(msg);
        this.result.setErrCode(code);
        return this.result;
    }

    public static <T> Result<T> data(T t){
        return new ResultUtil<T>().setData(t);
    }

    public static <T> Result<T> data(T t, String msg){
        return new ResultUtil<T>().setData(t, msg);
    }

    public static <T> Result<T> success(String msg){
        return new ResultUtil<T>().setSuccessMsg(msg);
    }

    public static <T> Result<T> error(String msg){
        return new ResultUtil<T>().setErrorMsg(msg);
    }

    public static <T> Result<T> error(Integer code, String msg){
        return new ResultUtil<T>().setErrorMsg(code, msg);
    }
}
