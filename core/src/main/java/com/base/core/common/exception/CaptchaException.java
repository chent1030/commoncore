package com.base.core.common.exception;

import lombok.Data;

/**
 * 验证码异常
 * @author chentao
 */
@Data
public class CaptchaException extends RuntimeException {

    private String errMsg;

    public CaptchaException(String errMsg) {
        super(errMsg);
        this.errMsg = errMsg;
    }
}
