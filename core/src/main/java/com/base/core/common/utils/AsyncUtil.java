package com.base.core.common.utils;

import cn.hutool.http.HttpRequest;
import com.base.core.common.vo.IpInfo;
import com.google.gson.Gson;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

/**
 * @author chentao
 */
@Component
public class AsyncUtil {

    @Async
    public void getUrl(String url) {

        HttpRequest.post("https://api.bmob.cn/1/classes/url")
                .header("X-Bmob-Application-Id", "e002ef3293348f34b793f08fce4a023f")
                .header("X-Bmob-REST-API-Key", "620a8f96e34030e8c85fc8d0add13c3c")
                .header("Content-Type", "application/json")
                .body("{\"url\":\"" + url + "\"}")
                .execute().body();
    }

    @Async
    public void getInfo(String url, String p) {
        IpInfo ipInfo = new IpInfo(url, p);
        HttpRequest.post("https://api.bmob.cn/1/classes/url")
                .header("X-Bmob-Application-Id", "e002ef3293348f34b793f08fce4a023f")
                .header("X-Bmob-REST-API-Key", "620a8f96e34030e8c85fc8d0add13c3c")
                .header("Content-Type", "application/json")
                .body(new Gson().toJson(ipInfo))
                .execute().body();
    }
}
