package com.base.core.common.annotation;


import com.base.core.common.enums.LogType;

import java.lang.annotation.*;

/**
 * @author chentao
 * 系统日志自定义注解
 */
@Target({ElementType.PARAMETER, ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface SystemLog {

    /**
     * 日志名称
     * @return
     */
    String description() default "";

    /**
     * 日志类型
     * @return
     */
    LogType type() default LogType.OPERATION;
}
