package com.base.core.common.enums;

/**
 * @author chentao
 */

public enum SecurityConstant {

    /**
     * token分割
     */
    TOKEN_SPLIT("Bearer "),

    /**
     * JWT签名加密key
     */
    JWT_SIGN_KEY("demo"),

    /**
     * token参数头
     */
    HEADER("accessToken"),

    /**
     * appToken参数头
     */
    APP_HEADER("appToken"),

    /**
     * 权限参数头
     */
    AUTHORITIES("authorities"),

    /**
     * 用户选择JWT保存时间参数头
     */
    SAVE_LOGIN("saveLogin"),

    /**
     * qq保存state前缀key
     */
    QQ_STATE("BASE_QQ:"),

    /**
     * 微博保存state前缀key
     */
    WEIBO_STATE("BASE_WEIBO:"),

    /**
     * 交互token前缀key
     */
    TOKEN_PRE("BASE_TOKEN_PRE:"),

    /**
     * 用户token前缀key 单点登录使用
     */
    USER_TOKEN("BASE_USER_TOKEN:"),

    /**
     * 会员交互token前缀key
     */
    TOKEN_MEMBER_PRE("BASE_TOKEN_MEMBER_PRE:"),

    /**
     * 会员token前缀key
     */
    MEMBER_TOKEN("BASE_MEMBER_TOKEN:");

    private String value;

    SecurityConstant(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}
