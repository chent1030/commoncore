package com.base.core.common.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author chentao
 */
@Data
public class SearchVO {

    @ApiModelProperty(value = "起始日期")
    private String startDate;

    @ApiModelProperty(value = "结束日期")
    private String endDate;
}
