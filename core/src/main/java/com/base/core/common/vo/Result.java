package com.base.core.common.vo;

import lombok.Data;

import java.io.Serializable;

/**
 * @author chentao
 */
@Data
public class Result<T> implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 返回代码
     */
    private int errCode;

    /**
     * 消息
     */
    private String errMsg;

    /**
     * 时间戳
     */
    private long timestamp = System.currentTimeMillis();

    /**
     * 结果对象
     */
    private T result;
}
