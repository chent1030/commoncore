package com.base.core.common.exception;

import lombok.Data;

/**
 * @author chentao
 */
@Data
public class BaseException extends RuntimeException {

    private String errMsg;

    public BaseException(String errMsg) {
        super(errMsg);
        this.errMsg = errMsg;
    }
}
