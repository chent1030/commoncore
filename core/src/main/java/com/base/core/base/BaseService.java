package com.base.core.base;

import java.io.Serializable;
import java.util.List;

/**
 * @author chentao
 */
@FunctionalInterface
public interface BaseService<E, ID extends Serializable> {

    public BaseDao<E, ID> getRepository();

    /**
     * 根据ID获取
     * @param id 主键/ID
     * @return E实体
     */
    public default E findById(ID id){
        return getRepository().findById(id).orElse(null);
    }

    /**
     * 获取所有列表
     * @return 实体数组
     */
    public default List<E> findByAll(){
        return getRepository().findAll();
    }

    /**
     * 获取总数
     * @return 总数
     */
    public default Long findTotalCount() {
        return getRepository().count();
    }

    /**
     * 保存
     * @param entity 实体
     * @return E
     */
    public default E save(E entity) {
        return getRepository().save(entity);
    }

    /**
     * 保存并刷新
     * @param entity 实体
     * @return E
     */
    public default E saveAndFlush(E entity) {
        return getRepository().saveAndFlush(entity);
    }

    /**
     * 批量保存
     * @param entities 实体数组
     * @return Iterable<E>
     */
    public default Iterable<E> saveAll(Iterable<E> entities) {
        return getRepository().saveAll(entities);
    }

    /**
     * 删除
     * @param entity 实体
     */
    public default void delete(E entity) {
        getRepository().delete(entity);
    }

    /**
     * 根据Id删除
     * @param id
     */
    public default void deleteById(ID id) {
        getRepository().deleteById(id);
    }

    /**
     * 批量删除
     * @param entities 实体数组
     */
    public default void delete(Iterable<E> entities) {
        getRepository().deleteInBatch(entities);
    }

    /**
     * 清空缓存，提交持久化
     */
    public default void flush() {
        getRepository().flush();
    }
}
